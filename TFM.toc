\babel@toc {english}{}
\babel@toc {spanish}{}
\babel@toc {english}{}
\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducción e historia de las conjeturas}{2}{section.1}%
\contentsline {section}{\numberline {2}La función zeta}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Series de potencias}{4}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Cuerpos finitos}{5}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}La función zeta}{6}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Las conjecturas de Weil}{11}{subsection.2.4}%
\contentsline {section}{\numberline {3}El caso de hipersuperficies diagonales}{16}{section.3}%
\contentsline {subsection}{\numberline {3.1}Caracteres, sumas de Gauss y Jacobi}{16}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}La relación de Hasse-Davenport}{24}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Cálculo de $\mathcal {N}_s$ y conjeturas para hipersuperficies diagonales}{27}{subsection.3.3}%
\contentsline {section}{\numberline {4}Conjeturas de Weil para curvas}{32}{section.4}%
\contentsline {subsection}{\numberline {4.1}Divisores}{32}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Racionalidad y ecuación funcional}{35}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Intersecciones en superficies}{38}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}La desigualdad de Hasse-Weil}{40}{subsection.4.4}%
\contentsline {section}{\numberline {5}Conclusiones}{42}{section.5}%
