from sage.all import *


s_max = 200
k = GF(2)
E = EllipticCurve(k,[1,1,1,1,0])
A = s_max*[None]
Columna2 = ['&' for i in range(1,s_max)]
for s in range(1,s_max):
    A[s] = E.cardinality(extension_degree = s)
columns = [range(1,s_max),A[1:]]
print(table(columns=[range(1,s_max), Columna2, A[1:]]))
